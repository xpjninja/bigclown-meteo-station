#include <bcl.h>
#include "bc_rtc_ds3231.h"

uint8_t dec_to_bcd(uint8_t val) {
    return ((val / 10 * 16) + (val % 10));
}

uint8_t bcd_to_dec(uint8_t val) {
    return ((val / 16 * 10) + (val % 16));
}

void bc_rtc_ds3231_init(bc_rtc_ds3231_t *self, bc_i2c_channel_t i2c_channel, uint8_t i2c_address) {
    memset(self, 0, sizeof(*self));

    self->_i2c_channel = i2c_channel;
    self->_i2c_address = i2c_address;

    bc_i2c_init(self->_i2c_channel, BC_I2C_SPEED_400_KHZ);
}

void bc_rtc_ds3231_get_datetime(bc_rtc_ds3231_t *self,
                                bc_datetime_t *datetime) {

    bc_i2c_memory_transfer_t transfer;

    transfer.device_address = self->_i2c_address;
    transfer.memory_address = 0x00;
    transfer.buffer = &self->buffer;
    transfer.length = 7;

    bc_i2c_memory_write_8b(self->_i2c_channel, self->_i2c_address, 0x0, 0x0);
    datetime->valid = bc_i2c_memory_read(self->_i2c_channel, &transfer);

    datetime->second = bcd_to_dec(self->buffer[0]);
    datetime->minute = bcd_to_dec(self->buffer[1]);
    datetime->hour = bcd_to_dec(self->buffer[2]);
    datetime->dayOfWeek = bcd_to_dec(self->buffer[3]);
    datetime->dayOfMonth = bcd_to_dec(self->buffer[4]);
    datetime->month = bcd_to_dec(self->buffer[5]);
    datetime->year = bcd_to_dec(self->buffer[6]);
}
