#include <application.h>
#include <math.h>

#define SERVICE_INTERVAL_INTERVAL (60 * 60 * 1000)
#define BATTERY_UPDATE_INTERVAL (60 * 60 * 1000)

#define TEMPERATURE_PUB_NO_CHANGE_INTERVAL_MS (15 * 60 * 1000)
#define TEMPERATURE_PUB_VALUE_CHANGE 0.2f
#define TEMPERATURE_UPDATE_SERVICE_INTERVAL (5 * 1000)
#define TEMPERATURE_UPDATE_NORMAL_INTERVAL (10 * 1000)

#define SET_TEMPERATURE_PUB_INTERVAL 15 * 60 * 1000;

#define COLOR_BLACK true

bc_gfx_t *gfx;
bc_led_t led;
bc_led_t led_lcd_red;
bc_led_t led_lcd_blue;
bc_tmp112_t tmp112;
bc_rtc_ds3231_t rtc_ds3231;
bc_datetime_t datetime;

values_t measured_values = {
        .temperature_outdoor_roof = NAN,
        .temperature_outdoor = NAN,
        .temperature_indoor = NAN
};

event_param_t temperature_event_param = {.next_pub = 0, .value = NAN};
bc_scheduler_task_id_t app_task_id = 0;

void temperature_outdoor_set(uint64_t *id, const char *topic, void *value, void *param);

void temperature_outdoor_roof_set(uint64_t *id, const char *topic, void *value, void *param);

void temperature_indoor_set(uint64_t *id, const char *topic, void *value, void *param);

void update_lcd();

static const bc_radio_sub_t subscribes[] = {
        {"meteo/-/temp/outdoor-roof", BC_RADIO_SUB_PT_FLOAT, temperature_outdoor_roof_set, NULL},
        {"meteo/-/temp/outdoor",      BC_RADIO_SUB_PT_FLOAT, temperature_outdoor_set,      NULL},
        {"meteo/-/temp/indoor",       BC_RADIO_SUB_PT_FLOAT, temperature_indoor_set,       NULL}
};

void temperature_outdoor_set(uint64_t *id, const char *topic, void *value, void *param) {
    measured_values.temperature_outdoor = *(float *) value;
    bc_log_info("Setting outdoor temp %.1f", measured_values.temperature_outdoor);
    update_lcd();
}

void temperature_outdoor_roof_set(uint64_t *id, const char *topic, void *value, void *param) {
    measured_values.temperature_outdoor_roof = *(float *) value;
    bc_log_info("Setting outdoor roof temp %.1f", measured_values.temperature_outdoor_roof);
    update_lcd();
}

void temperature_indoor_set(uint64_t *id, const char *topic, void *value, void *param) {
    measured_values.temperature_indoor = *(float *) value;
    bc_log_info("Setting indoor temp %.1f", measured_values.temperature_indoor);
    update_lcd();
}

void battery_event_handler(bc_module_battery_event_t event, void *event_param) {
    (void) event_param;

    float voltage;

    if (event == BC_MODULE_BATTERY_EVENT_UPDATE) {
        if (bc_module_battery_get_voltage(&voltage)) {
            bc_radio_pub_battery(&voltage);
        }
    }
}

void tmp112_event_handler(bc_tmp112_t *self, bc_tmp112_event_t event, void *event_param) {
    float value;
    event_param_t *param = (event_param_t *) event_param;

    if (event == BC_TMP112_EVENT_UPDATE) {
        if (bc_tmp112_get_temperature_celsius(self, &value)) {
            if ((fabsf(value - param->value) >= TEMPERATURE_PUB_VALUE_CHANGE) ||
                (param->next_pub < bc_scheduler_get_spin_tick())) {
                bc_radio_pub_temperature(BC_RADIO_PUB_CHANNEL_R1_I2C0_ADDRESS_DEFAULT, &value);

                param->value = value;
                param->next_pub = bc_scheduler_get_spin_tick() + TEMPERATURE_PUB_NO_CHANGE_INTERVAL_MS;
            }
        }
    }
}

void application_init(void) {

    bc_log_init(BC_LOG_LEVEL_DUMP, BC_LOG_TIMESTAMP_ABS);
    bc_log_info("Initializing Meteo Station");

    bc_module_battery_init();
    bc_module_battery_set_event_handler(battery_event_handler, NULL);
    bc_module_battery_set_update_interval(BATTERY_UPDATE_INTERVAL);

    bc_tmp112_init(&tmp112, BC_I2C_I2C0, 0x49);
    bc_tmp112_set_event_handler(&tmp112, tmp112_event_handler, &temperature_event_param);
    bc_tmp112_set_update_interval(&tmp112, TEMPERATURE_UPDATE_SERVICE_INTERVAL);

    bc_module_lcd_init();
    gfx = bc_module_lcd_get_gfx();

    bc_radio_init(BC_RADIO_MODE_NODE_LISTENING);
    bc_radio_set_rx_timeout_for_sleeping_node(2000);
    bc_radio_set_subs((bc_radio_sub_t *) subscribes, sizeof(subscribes) / sizeof(bc_radio_sub_t));
    bc_radio_pairing_request(FIRMWARE, VERSION);

    bc_led_init_virtual(&led_lcd_red, BC_MODULE_LCD_LED_RED, bc_module_lcd_get_led_driver(), true);
    bc_led_init_virtual(&led_lcd_blue, BC_MODULE_LCD_LED_BLUE, bc_module_lcd_get_led_driver(), true);
    bc_led_init(&led, BC_GPIO_LED, false, false);

    bc_rtc_ds3231_init(&rtc_ds3231, BC_I2C_I2C0, DS3231_I2C_ADDRESS);
    bc_rtc_ds3231_get_datetime(&rtc_ds3231, &datetime);
    bc_log_info("Current date time: %02d:%02d:%02d %2d.%2d.20%02d (%svalid)",
                datetime.hour, datetime.minute, datetime.second,
                datetime.dayOfMonth, datetime.month, datetime.year,
                datetime.valid ? "" : "in");

    bc_log_info("Meteo station initialized");
}

void application_task(void) {
}

void show_updated_datetime() {
    static char rendered_string[16];

    bc_rtc_ds3231_get_datetime(&rtc_ds3231, &datetime);
    bc_log_info("Current date time: %02d:%02d:%02d %2d.%2d.20%02d (%svalid)",
                datetime.hour, datetime.minute, datetime.second,
                datetime.dayOfMonth, datetime.month, datetime.year,
                datetime.valid ? "" : "in");
    sprintf(rendered_string, "%2d.%2d.20%02d %02d:%02d",
            datetime.dayOfMonth, datetime.month, datetime.year,
            datetime.hour, datetime.minute);
    bc_module_lcd_set_font(&bc_font_ubuntu_11);
    int w = bc_gfx_calc_string_width(gfx, rendered_string);
    bc_module_lcd_draw_string((128 - w) / 2, 116, rendered_string, COLOR_BLACK);

}

void lcd_temperature_house(void) {
    static char rendered_string[10];

    if (!bc_module_lcd_is_ready()) {
        return;
    }

    bc_system_pll_enable();

    bc_module_lcd_clear();

    bc_module_lcd_draw_line(3, 54, 39, 18, COLOR_BLACK);
    bc_module_lcd_draw_line(2, 54, 39, 17, COLOR_BLACK);
    bc_module_lcd_draw_line(8, 50, 39, 19, COLOR_BLACK);
    bc_module_lcd_draw_line(39, 18, 75, 54, COLOR_BLACK);
    bc_module_lcd_draw_line(39, 17, 76, 54, COLOR_BLACK);
    bc_module_lcd_draw_line(39, 19, 70, 50, COLOR_BLACK);
    bc_module_lcd_draw_rectangle(6, 51, 71, 100, COLOR_BLACK);
    bc_module_lcd_draw_rectangle(7, 52, 70, 99, COLOR_BLACK);
    bc_module_lcd_draw_rectangle(71, 65, 123, 100, COLOR_BLACK);

    bc_module_lcd_set_font(&bc_font_ubuntu_15);

    int w, x;
    if (!isnan(measured_values.temperature_indoor)) {
        sprintf(rendered_string, "%.1f %c%cC", measured_values.temperature_indoor, 0xC2, 0xB0);
        w = bc_gfx_calc_string_width(gfx, rendered_string);
        x = 7 + (70 - 7 - w) / 2;
        bc_module_lcd_draw_string(x, 72, rendered_string, COLOR_BLACK);
    }

    if (!isnan(measured_values.temperature_outdoor)) {
        sprintf(rendered_string, "%.1f %c%cC", measured_values.temperature_outdoor, 0xC2, 0xB0);
        w = bc_gfx_calc_string_width(gfx, rendered_string);
        x = 72 + (122 - 72 - w) / 2;
        bc_module_lcd_draw_string(x, 75, rendered_string, COLOR_BLACK);
    }

    if (!isnan(measured_values.temperature_outdoor_roof)) {
        sprintf(rendered_string, "%.1f %c%cC", measured_values.temperature_outdoor_roof, 0xC2, 0xB0);
        w = bc_gfx_calc_string_width(gfx, rendered_string);
        x = 76 + (127 - 76 - w) / 2;
        bc_module_lcd_draw_string(x, 47, rendered_string, COLOR_BLACK);
    }

    show_updated_datetime();

    bc_module_lcd_update();

    bc_system_pll_disable();
}

void update_lcd(void) {
    lcd_temperature_house();
}