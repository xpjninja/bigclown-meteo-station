#ifndef BIGCLOWN_METEO_STATION_BC_RTC_DS3231_H
#define BIGCLOWN_METEO_STATION_BC_RTC_DS3231_H

#define DS3231_I2C_ADDRESS 0x68

typedef struct bc_ds3231_t {
    bc_i2c_channel_t _i2c_channel;
    uint8_t _i2c_address;
    uint8_t buffer[7];
} bc_rtc_ds3231_t;

typedef struct bc_datetime_t {
    uint8_t second;
    uint8_t minute;
    uint8_t hour;
    uint8_t dayOfWeek;
    uint8_t dayOfMonth;
    uint8_t month;
    uint8_t year;

    bool valid;
} bc_datetime_t;

void bc_rtc_ds3231_init(bc_rtc_ds3231_t *self, bc_i2c_channel_t i2c_channel, uint8_t i2c_address);

void bc_rtc_ds3231_get_datetime(bc_rtc_ds3231_t *self, bc_datetime_t *datetime);

uint8_t dec_to_bcd(uint8_t val);

uint8_t bcd_to_dec(uint8_t val);

#endif //BIGCLOWN_METEO_STATION_BC_RTC_DS3231_H
