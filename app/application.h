#ifndef _APPLICATION_H
#define _APPLICATION_H


#ifndef VERSION
#define VERSION "dev"
#endif

#ifndef FIRMWARE
#define FIRMWARE "bigclown-meteo-station"
#endif

#include <bcl.h>
#include "bc_rtc_ds3231.h"

typedef struct
{
    uint8_t number;
    float value;
    bc_tick_t next_pub;

} event_param_t;

typedef struct
{
    float temperature_outdoor;
    float temperature_outdoor_roof;
    float temperature_indoor;
} values_t;

#endif
